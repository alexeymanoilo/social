import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { Error404Component } from './appComponents/error404/error404.component';
import { AppRouterModule } from  './appRouter/appRouter.module';
import { CoreModule } from './core/core.module';
@NgModule({
  declarations: [
    AppComponent,
    Error404Component,   
  ],
  imports: [
    BrowserModule,
    AppRouterModule,
    CoreModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
