import { MainComponent} from './main/main.component';
import { EditUserInfoComponent } from './edit-user-info/edit-user-info.component';
import { SearchComponent } from './search/search.component';
import { UserInfoComponent } from './user-info/user-info.component'
export const userRoutes = [
  { path: '', component: MainComponent , children: [
        { path: 'user/:name', component: UserInfoComponent },
        { path: 'edit', component: EditUserInfoComponent},
        { path: 'search', component : SearchComponent}
  ]}
]