import { Component, OnInit } from '@angular/core';
import { SingInService} from '../../common/services/index';
import { Router } from '@angular/router';
@Component({
  selector: 'navbar-left',
  templateUrl: './navbar-left.component.html',
  styleUrls: ['./navbar-left.component.scss']
})
export class NavbarLeftComponent implements OnInit {
  public userName : string = localStorage.getItem('currentUser');
  
  constructor(private singInService: SingInService, private router:Router) { 
   
  }
  ngOnInit() {
      
  }
  singOut(){
      this.singInService.logout();
      this.router.navigate(['welcome']);
  }
}
