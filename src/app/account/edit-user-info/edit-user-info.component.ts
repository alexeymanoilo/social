import { Component, OnInit } from '@angular/core';
import { User } from '../../common/models/user.model';
import { ApiService , EditUserProfileService} from '../../common/services/index';

@Component({
  selector: 'app-edit-user-info',
  templateUrl: './edit-user-info.component.html',
  styleUrls: ['./edit-user-info.component.scss']
})
export class EditUserInfoComponent implements OnInit {
  public load = false;
  public userInfo : User; 
  private file : any;
  public mouseoverLogin;
  public errorMassage;
  constructor(private apiService : ApiService , private editProfile : EditUserProfileService) { }

  ngOnInit() {
    var userName = localStorage.getItem('currentUser');
    console.log(`username - ${userName}`)
    this.apiService.get<User>(`api/users/${userName}`,true).subscribe(res =>{
      this.userInfo = res;
    })
  }
   edit(){
      this.load = true;
      let formData:FormData = new FormData();
      formData.append('uploadFile', this.file);
      formData.append("user",JSON.stringify(this.userInfo))      
      this.editProfile.upload(formData).subscribe(res => this.load = false);
  }
  onChange(image){
      this.file = image.srcElement.files[0];
  }
}
