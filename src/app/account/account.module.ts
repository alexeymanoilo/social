import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main/main.component';
import { userRoutes } from './routes'
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { UserInfoComponent } from './user-info/user-info.component';
import { EditUserInfoComponent } from './edit-user-info/edit-user-info.component';
import { SearchComponent } from './search/search.component';
import { NavbarLeftComponent } from './navbar-left/navbar-left.component';

@NgModule({
  imports: [
    RouterModule.forChild(userRoutes),   
    SharedModule
  ],
  declarations: [MainComponent, UserInfoComponent, EditUserInfoComponent, SearchComponent, NavbarLeftComponent]
})
export class AccountModule { }
