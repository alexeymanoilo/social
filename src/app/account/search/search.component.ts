import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../common/services/index';
import { User } from '../../common/models/user.model';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  public users : User[];
  public pageSize : number = 1000;
  private currectUserName = localStorage.getItem('currentUser');
  constructor(private apiService : ApiService) { }

  ngOnInit() {
     var users = this.apiService.get<User[]>(`api/users?PageSize=${this.pageSize}`,true ).subscribe(res =>{
        this.users = res;
      });
  }
  addFriend(userName){
      this.apiService.post(`api/friends?selfame=${this.currectUserName}&friendname=${userName}`, null , true).subscribe();
  }
  filter(value){
    var users = this.apiService.get<User[]>(`api/users?PageSize=${this.pageSize}&SearchQuery=${value}`,true ).subscribe(res =>{
        this.users = res;
      });
  }
}
