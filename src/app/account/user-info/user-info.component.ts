import { Component, OnInit , Inject} from '@angular/core';
import { User } from '../../common/models/user.model';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../common/services/index'
@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {
  public userInfo : User;
  public friends ;
  constructor( private apiService : ApiService, private route: ActivatedRoute ) {}
  ngOnInit() {
    var route;
    this.route
        .params
        .subscribe(params => {
            route = params['name'];
            this.getUserInfo(route);
            this.friends = this.apiService.get<User[]>(`api/friends?selfame=${params['name']}`,true );
    });
  }
  getUserInfo(route){
    this.apiService.get<User>('api/users/'+ route,true).subscribe(res =>{    
      if(!res.imagePath) res.imagePath = 'https://www.isupportcause.com/r/images/previewImage.png';
      if(!res.contactEmail) res.contactEmail = 'anonymous'
      this.userInfo = res;
    })
  }
  setDefaultValues(){

  }
}
