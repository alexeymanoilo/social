import { Injectable, Inject } from '@angular/core';
import { Http, Headers, RequestOptionsArgs, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import { BASE_URL } from './index';
import { SingUp } from '../models/toServer/singUp.model'
@Injectable()
export class EditUserProfileService {
  private baseUrl : string;
  constructor( @Inject(BASE_URL) private baseURL: string, private http: Http ) { 
    this.baseURL
  }

  public upload(formData:FormData){
   const auth = 'Authorization';
   const token = 'Bearer ' + localStorage.getItem('accessToken');
   const headers = new Headers({
         [auth]: token,
    }); 
    var options = new RequestOptions({headers});
    console.log(token);
   return this.http.put(`${this.baseURL}api/users`,formData,options )  
       .map((response: Response) => {  
            return response;                 
        })
  }
}