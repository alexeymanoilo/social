import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams} from '@angular/http';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import { ApiService, BASE_URL } from './index';
import { SingUp } from '../models/toServer/singUp.model'
@Injectable()
export class SingUpService {
  private baseUrl : string;
  constructor(private apiService : ApiService, @Inject(BASE_URL) private baseURL: string) { 
    this.baseURL
  }

  public singUp(userData : SingUp){
    let body = new URLSearchParams();
    body.set('UserName', userData.userName);
    body.set('Password', userData.password);
    body.set('ConfirmPassword', userData.confirmPassword);
    return this.apiService.post('api/Account/Register',body);
  }
}