import { Injectable , Inject} from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { BASE_URL, ApiService } from '../services/index';
@Injectable()
export class SingInService {
    private token: string;
    private baseUrl : string;

    constructor(private http: Http , @Inject(BASE_URL) private baseURL: string , private apiServe : ApiService ) {
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
        this.baseUrl = baseURL;
    }

    login(username: string, password: string): Observable<boolean> {

            let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded'});
            headers.append('Access-Control-Allow-Origin',this.baseUrl);
            headers.append('Access-Control-Allow-Methods', 'POST');
            const body = 'grant_type=password&username=' + username + '&password=' + password;
            return this.http.post(this.baseURL + 'token', body ,headers)
                .map((response: Response) => {
                    
                    let token = response.json() && response.json().access_token;
                    if (token) {
                        this.token = token;
                        console.log(token);
                        localStorage.setItem('currentUser',username);    
                        localStorage.setItem('accessToken',token);          
                        return true;
                    } else {
                        return false;
                    }
                });
    }

    logout(): void {
        this.token = null;
        localStorage.removeItem('currentUser');
        localStorage.removeItem('accessToken');  
    }
    
    getToken(){
        return this.token;
    }

}