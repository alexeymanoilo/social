import { Injectable, Inject } from '@angular/core';
import { Http, Headers, RequestOptionsArgs, Response, RequestOptions } from '@angular/http';
import { BASE_URL } from './app.tokens';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/do';
import  {SingInService } from './index';
export interface ApiRequestOptions {
  failSilently?: boolean;
  requestOptions?: RequestOptionsArgs;
}

let DEFAULT_API_OPTIONS: Headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded'});

@Injectable()
export class ApiService {
  private apiHost: string ;

  constructor(private http: Http, @Inject(BASE_URL) private baseURL: string) {
      this.apiHost = baseURL;
       
  }

  public AddAuthorizationToken( token ){

          const auth = 'Authorization';
          const newtoken = 'Bearer ' + token;
          const headers = new Headers({
            [auth]: token,
            'Content-Type': 'application/json'
          });
  }

  public get<T>(path: string, auth : boolean = false): Observable<T> {
    let options = !auth ? this.getRequestOptionsNoAuth() : this.getRequestOptions()
    return this.http.get(`${this.apiHost}${path}`, options)
      .map((res: Response) => <T>res.json())
      .catch((err: Response) => {
        return Observable.throw(err);
      });
  }

  public post(path: string, body: any = undefined,
              auth : boolean = false ): Observable<Response> {
    let options = !auth ? this.getRequestOptionsNoAuth() : this.getRequestOptions()
    return this.http.post(`${this.apiHost}${path}`, body, options)
      .map((res: Response) => res)
      .catch((err: Response) => {
        return Observable.throw(err);
      });
  }

  public put(path: string, body: any = undefined,
              auth : boolean = false): Observable<Response> {
    let options = !auth ? this.getRequestOptionsNoAuth() : this.getRequestOptions()           
    return this.http.put(`${this.apiHost}${path}`, body, options )
      .map((res: Response) => res)
      .catch((err: Response) => {
        return Observable.throw(err);
      });
  }

  // public delete(path: string, options: ApiRequestOptions = DEFAULT_API_OPTIONS): Observable<Response> {
  //   return this.http.delete(`${this.apiHost}${path}`, this.getRequestOptions(options))
  //     .map((res: Response) => res)
  //     .catch((err: Response) => {
  //       return Observable.throw(err);
  //     });
  // }

  // public patch(path: string, body: string, options: ApiRequestOptions = DEFAULT_API_OPTIONS): Observable<Response> {
  //   return this.http.patch(`${this.apiHost}${path}`, body, this.getRequestOptions(options))
  //     .map((res: Response) => res)
  //     .catch((err: Response) => {
  //       return Observable.throw(err);
  //     });
  // }

  // public head(path: string, options: ApiRequestOptions = DEFAULT_API_OPTIONS): Observable<Response> {
  //   return this.http.head(`${this.apiHost}${path}`, this.getRequestOptions(options))
  //     .map((res: Response) => res)
  //     .catch((err: Response) => {
  //       return Observable.throw(err);
  //     });
  // }

  private getRequestOptions(): RequestOptions {
    const auth = 'Authorization';
    const token = 'Bearer ' + localStorage.getItem('accessToken');
    const headers = new Headers({
      [auth]: token,
      'Content-Type': 'application/json'
    }); 
   return new RequestOptions({headers});
  }

  private getRequestOptionsNoAuth(): RequestOptions {
    const newOptions = new RequestOptions();
    let defaultOptions = new RequestOptions(new Headers({'Content-Type': 'application/json'}));
    defaultOptions = defaultOptions.merge(newOptions);

    return defaultOptions;
  }
}
