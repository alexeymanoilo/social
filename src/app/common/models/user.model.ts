export interface User {
     userName: string,
     contactEmail: string,
     imagePath: string
}