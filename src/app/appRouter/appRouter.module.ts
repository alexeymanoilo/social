import { NgModule } from '@angular/core';
import { RouterModule,Routes} from '@angular/router';
import { Error404Component } from '../appComponents/error404/error404.component'
import { AuthGuard } from '../common/guards';
const appRoutes:Routes = [
  { path: '', redirectTo: '/welcome', pathMatch: 'full'},
  { path: 'welcome', loadChildren: 'app/welcome/welcome.module#WelcomeModule'},
  { path: 'account', loadChildren: 'app/account/account.module#AccountModule', canActivate: [AuthGuard] },
  { path: '404', component: Error404Component },
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  declarations: [],
  providers : [],
  exports: [RouterModule]
})
export class AppRouterModule { }