import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BASE_URL,ApiService, SingUpService, SingInService, EditUserProfileService } from '../common/services/index'
import { HttpModule } from '@angular/http';
import { AuthGuard } from '../common/guards/index';
@NgModule({
  imports: [
    HttpModule
  ],
  declarations: []
})
export class CoreModule { 
    static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        { provide: BASE_URL, useValue: "http://socialbitcoin.azurewebsites.net/"},
        ApiService,
        SingUpService,
        SingInService,
        EditUserProfileService,
        AuthGuard
      ]
    };
  }
}
