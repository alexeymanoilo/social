import { Component, OnInit } from '@angular/core';
import { SingInService } from '../../common/services/index';
import { Router } from '@angular/router';
@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.css']
})
export class AuthenticationComponent implements OnInit {
  public load : boolean = false;
  public mouseoverLogin = false;
  public userName : string;
  public password : string;
  public fail : boolean = false;
  private _singInService : SingInService;
  constructor(private singInService : SingInService, private router:Router) {
    this._singInService = singInService;
   }

  ngOnInit() {
  }

  login(formValues){
      this.load = true;
      this._singInService.login(formValues.userName, formValues.password)
      .subscribe(res => {
        if(res){
          var route =  'account/user/' + formValues.userName;
          this.router.navigate([route]);
        }
        else{
          this.fail = true ;      
        }
      }, err => this.load = false );
  }
}
