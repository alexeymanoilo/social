import { MainComponent} from './main/main.component';
import { InfoComponent} from './info/info.component';
import { RegistrationComponent } from './registration/registration.component';
import { AuthenticationComponent } from './authentication/authentication.component';

export const userRoutes = [
  {path: '', component: MainComponent, children: [
  { path: '', component: InfoComponent },
  { path: 'singUp', component: RegistrationComponent},
  { path: 'singIn', component : AuthenticationComponent}
  ]}
]