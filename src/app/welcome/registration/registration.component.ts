import { error } from 'util';
import { Component, OnInit } from '@angular/core';
import { SingUpService } from '../../common/services/index';
import { Router } from '@angular/router';
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  public load : boolean = false;
  public errorMassage : boolean = false;
  public mouseoverLogin ;
  public userName;
  public password;
  public confirmPassword;
  constructor(private service : SingUpService, private router:Router ) { }

  ngOnInit() {
        }
  singUp(values){
    this.load = true;
    this.service.singUp(values).subscribe(res => {
          this.errorMassage = false;
          this.router.navigate(['welcome/singIn']);         
    }
    , error =>{ 
      this.errorMassage = true; 
      this.load = false;
    });
  }
}
