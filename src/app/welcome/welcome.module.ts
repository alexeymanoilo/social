import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { MainComponent } from './main/main.component';
import { RegistrationComponent } from './registration/registration.component';
import { AuthenticationComponent } from './authentication/authentication.component';
import { InfoComponent } from './info/info.component';
import { userRoutes } from './routes'
import { RouterModule } from '@angular/router';
import { NavmenuComponent } from './navmenu/navmenu.component';

@NgModule({
  imports: [
    RouterModule.forChild(userRoutes),
    SharedModule
  ],
  declarations: [MainComponent, RegistrationComponent, AuthenticationComponent, InfoComponent, NavmenuComponent]
})
export class WelcomeModule { }
